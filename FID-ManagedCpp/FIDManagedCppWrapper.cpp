
#include "stdafx.h"
#pragma once

#include "FIDManagedCppWrapper.h"
#include "../FID-Cuda/FIDNativeCppWrapper.cuh"

using namespace System;
using namespace System::Numerics;
using namespace FID::ManagedCpp;
using namespace FID::NativeCpp;
using namespace FID::Cuda;

// ReSharper disable CppExpressionWithoutSideEffects

array<Complex, 2>^ FIDManagedCppWrapper::Convert(std::vector<std::complex<float>> toConvert)
{
	// Todo: Make all of these reinterpretation casts.

	const size_t nx = SLMResolutionX;
	const size_t ny = SLMResolutionY;
	const size_t SIZE = toConvert.size();
	if (nx * ny != SIZE) throw gcnew Exception("Incompatible image sizes!");
	array<Complex, 2>^ tempArr = gcnew array<Complex, 2>(ny, nx);

	for (size_t y = 0; y < ny; y++)
	{
		for (size_t x = 0; x < nx; x++)
		{
			const auto i = y * nx + x;
			tempArr[y, x] = Complex(toConvert[i].real(), toConvert[i].imag());
		}
	}
	return tempArr;
}

std::vector<std::complex<float>> FIDManagedCppWrapper::Convert(array<Complex, 2>^ toConvert)
{
	// Todo: Make all of these reinterpretation casts.

	const size_t nx = SLMResolutionX;
	const size_t ny = SLMResolutionY;
	const size_t SIZE = toConvert->Length;
	if (nx * ny != SIZE) throw gcnew Exception("Incompatible image sizes!");
	auto tempVec = std::vector<std::complex<float>>(SIZE);

	for (size_t y = 0; y < ny; y++)
	{
		for (size_t x = 0; x < nx; x++)
		{
			const auto i = y * nx + x;
			tempVec[i] = std::complex<float>(toConvert[y, x].Real, toConvert[y, x].Imaginary);
		}
	}
	return tempVec;
}

array<int, 2>^ FIDManagedCppWrapper::Convert(std::vector<int> toConvert)
{
	// Todo: Make all of these reinterpretation casts.

	const size_t nx = SLMResolutionX;
	const size_t ny = SLMResolutionY;
	const size_t SIZE = toConvert.size();
	if (nx * ny != SIZE) throw gcnew Exception("Incompatible image sizes!");
	array<int, 2>^ tempArr = gcnew array<int, 2>(ny, nx);

	for (size_t y = 0; y < ny; y++)
	{
		for (size_t x = 0; x < nx; x++)
		{
			const auto i = y * nx + x;
			tempArr[y, x] = toConvert[i];
		}
	}
	return tempArr;
}

std::vector<int> FIDManagedCppWrapper::Convert(array<int, 2>^ toConvert)
{
	// Todo: Make all of these reinterpretation casts.

	const size_t nx = SLMResolutionX;
	const size_t ny = SLMResolutionY;
	const size_t SIZE = toConvert->Length;
	if (nx * ny != SIZE) throw gcnew Exception("Incompatible image sizes!");
	auto tempVec = std::vector<int>(SIZE);

	for (size_t y = 0; y < ny; y++)
	{
		for (size_t x = 0; x < nx; x++)
		{
			const auto i = y * nx + x;
			tempVec[i] = toConvert[y, x];
		}
	}
	return tempVec;
}

// ReSharper restore CppExpressionWithoutSideEffects
