#pragma once

#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <complex>
#include <vector>
#include "../FID-Cuda/FIDNativeCppWrapper.cuh"

using namespace System;
using namespace System::Numerics;
using namespace System::Collections::Generic;
using namespace FID::NativeCpp;
using namespace FID::Cuda;
using namespace System::IO;
using namespace System::Runtime::InteropServices;

namespace FID
{
	namespace ManagedCpp
	{
		public delegate void LoggerDelegate(String^ msg);

		public ref class FIDManagedCppWrapper
		{
			FIDNativeCppWrapper* Wrap;

			int SLMResolutionX;
			int SLMResolutionY;

		public:

			explicit FIDManagedCppWrapper(
				LoggerDelegate^ logger,
				const size_t slmResolutionX,
				const size_t slmResolutionY) :
				Wrap(new FIDNativeCppWrapper(
					static_cast<LoggingCallback>(Marshal::GetFunctionPointerForDelegate(logger).ToPointer()),
					slmResolutionX,
					slmResolutionY)),
				SLMResolutionX(slmResolutionX),
				SLMResolutionY(slmResolutionY)
			{
				
			}

			~FIDManagedCppWrapper()
			{
				delete Wrap;
			}

			void ShowDemoReel()
			{
				Wrap->ShowDemoReel();
			}

			void SetImage(array<Complex, 2>^ image)
			{
				Wrap->SetImage(Convert(image));
			}

			array<Complex, 2>^ GetImage()
			{
				return Convert(Wrap->GetImage());
			}

		private:
			array<Complex, 2>^ Convert(std::vector<std::complex<float>> toConvert);
			std::vector<std::complex<float>> Convert(array<Complex, 2>^ toConvert);
			array<int, 2>^ Convert(std::vector<int> toConvert);
			std::vector<int> Convert(array<int, 2>^ toConvert);
		};
	}
}
#endif
