# README

This is a super easy demo of how to get Thrust and Cuda working with .NET langauges such as C# and VB. Ignore the whole FastImageDisplay/FID thing, that was just part of a setup I did for a friend.

While not strictly necessary, the 4 layers of wrappers make this significantly easier people unfamiliar with Cuda/C++/.NET interop to get things working. Structure is .NET code (CS/VB) <> Managed C++ <> Native C++ <> Cuda

Any questions, contact me on pjc209@cam.ac.uk - peterjchristopher@gmail.com - Peter J. Christopher