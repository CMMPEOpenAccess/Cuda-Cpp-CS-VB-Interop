﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FID.ManagedCpp;

namespace FID_CS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        public void Log(string s)
        {
            label1.Text += s + " ... ";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text += "Entering" + " ... ";
            var wrapper = new FIDManagedCppWrapper(Log, 100, 100);
            wrapper.ShowDemoReel();
            label1.Text += "Exiting" + " ... ";
        }
    }
}
