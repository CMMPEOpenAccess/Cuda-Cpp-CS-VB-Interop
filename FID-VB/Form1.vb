﻿
Imports FID.ManagedCpp

Public Class Form1

    Sub Log(s As String)
        Label1.Text += s + " ... "
    End Sub

    Public Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Label1.Text += "Entering" + " ... "
        Dim wrapper As New FIDManagedCppWrapper(New LoggerDelegate(AddressOf Log), 100, 100)
        wrapper.ShowDemoReel()
        Label1.Text += "Exiting" + " ... "
    End Sub
End Class
