#pragma once
#include "FIDCuda.cuh"
#include <vector>
#include <complex>
#include <thrust/complex.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/copy.h>
#include <vector>

using namespace FID::Cuda;

void FIDCuda::ShowDemoReel()
{
	Log("CMMPE Rulez!");
}

void FIDCuda::SetImage(const std::vector<std::complex<float>>& hostImage)
{
	Log("void FIDCuda::SetImage(const std::vector<std::complex<float>>& hostImage);");

	// Can reinterpret because thrust and std complex have identical memory layouts
	const auto host_mod = reinterpret_cast<const std::vector<thrust::complex<float>>*>(&hostImage);
	Image = thrust::device_vector<thrust::complex<float>>(*host_mod);
}

std::vector<std::complex<float>> FIDCuda::GetImage()
{
	Log("std::vector<std::complex<float>> FIDCuda::GetImage();");

	// Can reinterpret because thrust and std complex have identical memory layouts
	thrust::host_vector<thrust::complex<float>> host_mod = Image;
	std::vector<std::complex<float>> result(host_mod.size());
	thrust::copy(
		host_mod.begin(),
		host_mod.end(),
		result.begin());
	return result;
}