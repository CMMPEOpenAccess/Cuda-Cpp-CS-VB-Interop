
#pragma once
#include "FIDCuda.cuh"
#include "FIDNativeCppWrapper.cuh"

using namespace FID::Cuda;
using namespace FID::NativeCpp;

FIDNativeCppWrapper::FIDNativeCppWrapper(
	LoggingCallback logger,
	const size_t slmResolutionX,
	const size_t slmResolutionY) :
	Wrap(new FIDCuda(
		logger,
		slmResolutionX,
		slmResolutionY))
{
}

FIDNativeCppWrapper::~FIDNativeCppWrapper()
{
	delete Wrap;
}

void FIDNativeCppWrapper::ShowDemoReel()
{
	Wrap->ShowDemoReel();
}

void FIDNativeCppWrapper::SetImage(const std::vector<std::complex<float>>& image)
{
	return Wrap->SetImage(image);
}

std::vector<std::complex<float>> FIDNativeCppWrapper::GetImage()
{
	return Wrap->GetImage();
}