#pragma once

#include <complex>
#include <vector>
#include "LoggingCallback.h"


namespace FID
{
	namespace Cuda
	{
		class FIDCuda;
	}

	namespace NativeCpp
	{
		using namespace FID::Cuda;

		class FIDNativeCppWrapper
		{
			FIDCuda* Wrap;

		public:

			FIDNativeCppWrapper(
				LoggingCallback logger,
				const size_t slmResolutionX,
				const size_t slmResolutionY);

			virtual ~FIDNativeCppWrapper();

			void ShowDemoReel();

			void SetImage(const std::vector<std::complex<float>>& image);

			std::vector<std::complex<float>> GetImage();
		};
	}
}
